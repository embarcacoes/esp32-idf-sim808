#include "driver/gpio.h"
#include "driver/uart.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/projdefs.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "hal/gpio_types.h"
#include <stdio.h>
#include <string.h>

#define UART_NUM UART_NUM_1
#define UART_BAUD_RATE 9600
#define TXD_PIN (GPIO_NUM_16) // Pino de TX da UART
#define RXD_PIN (GPIO_NUM_17) // Pino de RX da UART
#define D9_PIN (GPIO_NUM_4)   // Pino do powerpin

const char *TAG = "gps"; // Tag de LOG

const int maxCh = 13;         // max Channel -> US = 11, EU = 13, Japan = 14
const int RX_BUF_SIZE = 1024; // Tamanho do buffer de leitura via UART
const int TX_BUF_SIZE = 25;   // Tamanho do buffer de escrita via UART
const int SEND_DATA_QUEUE_SIZE =
    10; // Tamanho da fila de dados a serem enviados

QueueHandle_t send_data_queue; // Fila de comunicação entre as tasks de sniffer
                               // e envio de dados

void init_uart(void);
int sendData(const char *logName, const char *data);
static void tx_task(void *arg);
static void rx_task(void *arg);
void init_sim808();
static void main_task(void *arg);

void app_main(void) {
    /*
    Função principal, responsavel por inicializar os modulos e criar todas as
    tasks
    */
    ESP_LOGI(TAG, "Iniciando...");

    init_uart(); // Configura a comunicação via UART
    xTaskCreate(rx_task, "uart_rx_task", 8192, NULL, configMAX_PRIORITIES - 1,
                NULL); // Cria a task de recepção de dados via UART
    xTaskCreate(tx_task, "uart_tx_task", 4096, NULL, configMAX_PRIORITIES - 2,
                NULL); // Cria a task de envio de dados via UART
    xTaskCreate(main_task, "main_task", 4096, NULL, configMAX_PRIORITIES - 3,
                NULL); // Cria a task principal
    ESP_LOGI(TAG, "Rotina de inicialização concluida...");
}

void init_uart(void) {
    /*
    Função responsavel por configurar e iniciar a UART
    */
    const uart_config_t uart_config = {
        // Struct com as confiugrações da UART
        .baud_rate = UART_BAUD_RATE,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_DEFAULT,
        .rx_flow_ctrl_thresh = 122,
    };
    uart_driver_install(UART_NUM, RX_BUF_SIZE * 2, 0, 0, NULL,
                        0); // Escolhe a UART a ser utilizada
    uart_param_config(
        UART_NUM,
        &uart_config); // Configura a UART com base dos parametros do struct
    uart_set_pin(UART_NUM, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE,
                 UART_PIN_NO_CHANGE); // Configura os pinos da UART

    send_data_queue = xQueueCreate(
        SEND_DATA_QUEUE_SIZE,
        sizeof(char) * TX_BUF_SIZE); // Cria uma fila de envio de dados
}

int sendData(const char *logName, const char *data) {
    /*
    Função responsavel por enviar dados via UART
    */
    const int len = strlen(data); // Obtenho o tamanho da string recebido
    const int txBytes =
        uart_write_bytes(UART_NUM_1, data, len); // Escreve os bytes na UART
    ESP_LOGI(logName, "Wrote %d bytes", txBytes);
    return txBytes; // Retorna a quantidade de bytes enviados
}

static void tx_task(void *arg) {
    /*
    Task responsavel por realizar o envio dos dados via UART
    */
    static const char *TX_TASK_TAG =
        "TX_TASK"; // Crio uma tag de log especifica para os dados enviados
    esp_log_level_set(TX_TASK_TAG,
                      ESP_LOG_INFO); // Seto com uma cor de log diferente
    char rxBuffer[TX_BUF_SIZE];      // Aloco o buffer para enviar informações
    while (1)                        // Repito infinitamente
    {
        if (xQueueReceive(
                send_data_queue, &(rxBuffer),
                portMAX_DELAY)) // Se tem algum dado na fila para ser enviado
        {
            sendData(TX_TASK_TAG, rxBuffer); // Envia os dados
        }
        vTaskDelay(50 / portTICK_PERIOD_MS); // Espera 50ms
    }
}

static void rx_task(void *arg) {
    /*
    Task responsavel por ouvir e receber os dados da UART
    */
    static const char *RX_TASK_TAG =
        "RX_TASK"; // Crio uma tag de log especifica para os dados recebidos
    esp_log_level_set(RX_TASK_TAG,
                      ESP_LOG_INFO); // Seto com uma cor de log diferente
    uint8_t *data = (uint8_t *)malloc(
        RX_BUF_SIZE + 1); // Aloco o buffer para receber informações
    while (1)             // Repito infinitamente
    {
        const int rxBytes = uart_read_bytes(
            UART_NUM_1, data, RX_BUF_SIZE,
            1000 / portTICK_PERIOD_MS); // Realiza a leitura do buffer da UART
        if (rxBytes > 0)                // Verifica se tem algo no buffer
        {
            data[rxBytes] =
                0; // Coloca a ultima posição do buffer após os dados como
                   // para sinalizar que ali e o fim da string
            ESP_LOGI(RX_TASK_TAG, "Read %d bytes: '%s'", rxBytes,
                     data); // Imprime os dados
            ESP_LOG_BUFFER_HEXDUMP(RX_TASK_TAG, data, rxBytes, ESP_LOG_INFO);
        }
    }
    free(data); // Desaloca o buffer
}

void init_sim808() {
    gpio_set_direction(D9_PIN, GPIO_MODE_OUTPUT);

    ESP_LOGI(TAG, "Iniciando SIM808");

    gpio_set_level(D9_PIN, 1);

    vTaskDelay(pdMS_TO_TICKS(1000));

    gpio_set_level(D9_PIN, 0);

    xQueueSend(send_data_queue, "AT+CGNSPWR=1\n", 1000);

    vTaskDelay(pdMS_TO_TICKS(3000));

    ESP_LOGI(TAG, "SIM808 Iniciado");

    ESP_LOGI(TAG, "Ligando GPS");

    xQueueSend(send_data_queue, "AT+CGPSPWR=1\n", 1000);

    vTaskDelay(pdMS_TO_TICKS(1000));

    xQueueSend(send_data_queue, "AT+CGPSSTATUS?\n", 1000);

    vTaskDelay(pdMS_TO_TICKS(1000));
}

static void main_task(void *arg) {

    init_sim808();

    while (1) {
        xQueueSend(send_data_queue, "AT+CGPSINF=0\n", 1000);

        vTaskDelay(pdMS_TO_TICKS(60000));
    }

    vTaskDelete(NULL);
}